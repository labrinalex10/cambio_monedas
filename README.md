# cambio_monedas

## Requisitos

1. Comando curl
2. Comando cat
3. Internet

## Primeros pasos

Creamos la carpeta oculta en el /home/${USER}/.monedas

$ mkdir ~/.monedas

Copiamos el archivo a $PATH (Puedes saber la ubicación con echo $PATH)

$ sudo cp monedas /usr/local/bin/

## Uso

Ejecutamos escribiendo la palabra monedas, muestra el tipo de cambio de diferentes monedas a soles, y permite calcular la compra o venta de estas.

$ monedas

## Monedas

Muestra el precio de compra y venta exactamente de 8 monedas: 

1. Dólar de N.A.
2. Dólar Canadiense
3. Peso Chileno
4. Libra Esterlina
5. Yen Japonés
6. Peso Mexicano
7. Franco Suizo
8. Euro
